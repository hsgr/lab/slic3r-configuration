# Slic3r Configuratios

In this repository are palced slic3r configuration files for HSGR 3D-printers.
Thank to [HSGR members](https://www.hackerspace.gr/) and [Lulzbot](https://www.lulzbot.com/)
these configuration files were made. The TAZ4 is refered to a modified 3D-printer
with E3D Hot-end with extrusion nozzle: 0.4mm. More about how to 
[print](https://www.hackerspace.gr/wiki/index.php?title=3D_Printing).
